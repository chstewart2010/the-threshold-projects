﻿using System;
using System.Collections;

namespace PrimeLibrary.Internal
{
    /// <summary>
    /// Provide a method for print primes via an Euler sieve (includes even numbers)
    /// </summary>
    internal class EulerSieveCollector : IPrimeCollector
    {
        private const int MaxValue = 2100000000;

        /// <inheritdoc/>
        public void UsePrimes(
            object ceiling,
            Action<IEnumerable> action)
        {
            if (MathHelper.MatchCeiling(ceiling, MaxValue, out var matchedCeiling))
            {
                action(BuildEulerSieve(matchedCeiling));
            }
        }

        private static IEnumerable BuildEulerSieve(int ceiling)
        {
            var sieve = GetBooleanArray(ceiling);
            yield return 2;

            for (int prime = 3; prime <= ceiling; prime += 2)
            {
                if (!sieve[prime])
                {
                    continue;
                }

                yield return prime;

                var startingComposite = prime * prime;
                for (var composite = startingComposite; composite < ceiling &&
                composite >= prime; composite += 2 * prime)
                {
                    sieve[composite] = false;
                }
            }
        }

        private static bool[] GetBooleanArray(int ceiling)
        {
            // default false for 0 and 1, true for 2
            bool[] array = new bool[ceiling + 1];
            array[0] = false;
            array[1] = false;
            array[2] = false;

            for (int i = 3; i <= ceiling; i++)
            {
                array[i] = i % 2 != 0;
            }

            return array;
        }
    }
}
