﻿using System;
using System.Collections;

namespace PrimeLibrary.Internal
{
    /// <summary>
    /// Provide a method for print primes via an Euler sieve
    /// </summary>
    internal class SundaramSieveCollector : IPrimeCollector
    {
        /// <inheritdoc/>
        public void UsePrimes(
            object ceiling,
            Action<IEnumerable> action)
        {
            if (MathHelper.MatchCeiling(ceiling, int.MaxValue - 1, out var matchedCeiling))
            {
                action(BuildSundaramSieve(matchedCeiling));
            }
        }

        private static IEnumerable BuildSundaramSieve(int ceiling)
        {
            var sieve = GetBooleanArray(ceiling);
            yield return 2;

            for (int prime = 3; prime < ceiling; prime += 2)
            {
                if (!sieve[(prime - 3) / 2])
                {
                    continue;
                }

                yield return prime;

                var startingComposite = prime * prime;
                for (var composite = startingComposite; composite < ceiling &&
                composite >= prime; composite += 2 * prime)
                {
                    sieve[(composite - 3) / 2] = false;
                }
            }
        }

        private static bool[] GetBooleanArray(int ceiling)
        {
            bool[] array = new bool[(ceiling - 1)/2];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = true;
            }

            return array;
        }
    }
}
