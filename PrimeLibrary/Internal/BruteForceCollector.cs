﻿using System;
using System.Collections;

namespace PrimeLibrary.Internal
{
    /// <summary>
    /// Provide a method for print primes via brute force
    /// </summary>
    internal class BruteForceCollector : IPrimeCollector
    {
        /// <inheritdoc/>
        public void UsePrimes(
            object ceiling,
            Action<IEnumerable> action)
        {
            if (MathHelper.MatchCeiling(ceiling, out var matchedCeiling))
            {
                action(GetPrimes(matchedCeiling));
            }
        }

        private static IEnumerable GetPrimes(long num)
        {
            yield return 2;
            for (long possiblePrime = 3; possiblePrime <= num; possiblePrime += 2)
            {
                if (IsPrime(possiblePrime))
                {
                    yield return possiblePrime;
                }
            }
        }

        private static bool IsPrime(long value)
        {
            for (long factor = 3; factor * factor <= value; factor += 2)
            {
                if (value % factor == 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
