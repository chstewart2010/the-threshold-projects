﻿using System;

namespace PrimeLibrary.Internal
{
    internal static class MathHelper
    {
        internal static bool MatchCeiling(
            object ceiling,
            int maxAllowedValue,
            out int matched)
        {
            if (ceiling is int matchedCeiling)
            {
                if (matchedCeiling > maxAllowedValue)
                {
                    throw new ArgumentOutOfRangeException
                    (
                        paramName: nameof(ceiling),
                        message: $"{matchedCeiling} is larger than the valid constraint for this prime collector"
                    );
                }

                matched = matchedCeiling > 0
                    ? matchedCeiling
                    : -matchedCeiling;
                return true;
            }

            throw new ArgumentException
            (
                message: $"{ceiling} is not of type 'int'",
                paramName: nameof(ceiling)
            );
        }

        internal static bool MatchCeiling(
            object ceiling,
            out long matched)
        {
            if (ceiling is long matchedLong)
            {
                matched = matchedLong > 0
                    ? matchedLong
                    : -matchedLong;
                return true;
            }

            if (ceiling is int matchedInt)
            {
                matched = matchedInt > 0
                    ? matchedInt
                    : -matchedInt;
                return true;
            }

            throw new ArgumentException
            (
                message: $"{ceiling} is not of type 'long'",
                paramName: nameof(ceiling)
            );
        }
    }
}
