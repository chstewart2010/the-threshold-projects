﻿using System;
using System.Collections;

namespace PrimeLibrary
{
    /// <summary>
    /// Provides a method for printing primes
    /// </summary>
    public interface IPrimeCollector
    {
        /// <summary>
        /// Calls an action for the user to use the primes however they want
        /// </summary>
        /// <param name="ceiling">The largest value to check</param>
        /// <param name="action">The method to perform on the prime collection</param>
        void UsePrimes(
            object ceiling,
            Action<IEnumerable> action);
    }
}
