﻿using PrimeLibrary.Internal;
using System;
using System.IO;

namespace PrimeLibrary
{
    /// <summary>
    /// Provides a method for building a <see cref="IPrimeCollector"/>
    /// </summary>
    public static class CollectorFactory
    {
        /// <summary>
        /// Returns a <see cref="IPrimeCollector"/> based the user input
        /// </summary>
        /// <param name="writer">Object to write options to</param>
        /// <param name="func">Function to retrieve input from</param>
        public static IPrimeCollector GetPrimeCollector(
            TextWriter writer,
            Func<int> func)
        {
            while (true)
            {
                PrintOptions(writer);
                var num = func();

                if (num >= 1 &&
                    num <= 3)
                {
                    return GetPrimeCollector(num);
                }

                writer.WriteLine();
            }
        }

        private static IPrimeCollector GetPrimeCollector(int option)
        {
            return option switch
            {
                1 => new BruteForceCollector(),
                2 => new EulerSieveCollector(),
                3 => new SundaramSieveCollector(),
                _ => throw new ArgumentOutOfRangeException(nameof(option), option, "Please select an value between 1 and 3"),
            };
        }

        private static void PrintOptions(TextWriter writer)
        {
            writer.WriteLine("Which method for finding primes would you like to use?\n" +
                "1. Brute Force checks. (effective for 2 <= input <= Int64.MaxValue)\n" +
                "2. Euler Sieve (all numbers). (effective for 2 <= input <= Int32.MaxValue/2)\n" +
                "3. Sundaram Sieve (only odd numbers). (effective 2 <= input < Int32.MaxValue)\n");
        }
    }
}
