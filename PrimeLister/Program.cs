﻿using System;
using System.Collections;
using PrimeLibrary;

namespace PrimeLister
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var printer = CollectorFactory.GetPrimeCollector
                 (
                    writer: Console.Out,
                    func: GetIntFromConsole
                 );

                Console.Write("Enter a number> ");
                var input = Console.ReadLine();

                if (int.TryParse(input, out var numAsInt) &&
                    numAsInt * numAsInt != numAsInt)
                {
                    printer.UsePrimes
                    (
                        ceiling: numAsInt,
                        action: PrintPrimes
                    );
                }
                else if (long.TryParse(input, out var numAsLong) &&
                    numAsLong * numAsLong != numAsLong)
                {
                    printer.UsePrimes
                    (
                        ceiling: numAsLong,
                        action: PrintPrimes
                    );
                }

                Console.WriteLine();
            }
        }

        static int GetIntFromConsole()
        {
            int.TryParse(Console.ReadLine(), out var num);
            return num;
        }

        static void PrintPrimes(IEnumerable primes)
        {
            var startTime = DateTime.Now;
            Console.Write("Primes> ");

            foreach (var prime in primes)
            {
                Console.Write($"{prime}, ");
            }

            Console.WriteLine("STOP");
            var endTime = DateTime.Now;
            Console.WriteLine($"Total write time: {(endTime - startTime).TotalMilliseconds} ms");
        }
    }
}
