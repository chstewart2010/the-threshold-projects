﻿using System;

namespace Quiz
{
    class Program
    {
        static void Main(string[] args)
        {
            TakeQuiz();
        }

        static void TakeQuiz()
        {
            while (true)
            {
                PrintList();
                int.TryParse(Console.ReadLine(), out var choice);
                switch (choice)
                {
                    case 1:
                        Quiz.AddQuestion();
                        break;
                    case 2:
                        Quiz.ListQuestions();
                        break;
                    case 3:
                        Quiz.RemoveQuestion();
                        break;
                    case 4:
                        Quiz.ModifyQuestion();
                        break;
                    case 5:
                        Quiz.GiveQuiz();
                        break;
                    case 6:
                        Quiz.FlushQuiz();
                        break;
                    case 7:
                        return;
                    default:
                        Console.WriteLine("Please choose a number between 1 to 7.");
                        break;
                }
            }
        }

        static void PrintList()
        {
            Console.WriteLine($"What would you like to do? (Current # of Questions: ({Quiz.Questions.Count}) (Total Allowed: {Quiz.MaxQuestions})");
            Console.WriteLine("1. Add a question to the quiz.");
            Console.WriteLine("2. List Quiz questions.");
            Console.WriteLine("3. Remove a question from the quiz.");
            Console.WriteLine("4. Modify a question in the quiz.");
            Console.WriteLine("5. Take a quiz.");
            Console.WriteLine("6. Flush quiz questions.");
            Console.WriteLine("7. Quit.");
        }
    }
}
