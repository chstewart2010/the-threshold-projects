﻿namespace Quiz
{
    class Question
    {
        public enum DifficultyContainer
        {
            Easy = 1,
            Normal = 2,
            Hard = 3
        }
        public string Text { get; }
        public string Answer { get; }
        public DifficultyContainer Difficulty { get; }

        public Question(string question, string answer, int difficulty)
        {
            Text = question;
            Answer = answer;
            Difficulty = (DifficultyContainer)difficulty;
        }
    }
}
