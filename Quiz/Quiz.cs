﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Quiz
{
    static class Quiz
    {
        public const int MaxQuestions = 100;
        public static readonly List<Question> Questions = new List<Question>();

        /// <summary>
        /// Adds a new question to the quiz
        /// </summary>
        public static void AddQuestion() => AddQuestion(-1);

        /// <summary>
        /// Modifies a question in the quiz
        /// </summary>
        public static void ModifyQuestion()
        {
            if (HasAnyQuestions())
            {
                ListQuestions();
                AddQuestion(GetQuestionIndex());
            }
        }

        /// <summary>
        /// Removes a question from the quiz
        /// </summary>
        public static void RemoveQuestion()
        {
            if (HasAnyQuestions())
            {
                ListQuestions();
                Questions.RemoveAt(GetQuestionIndex());
            }
        }

        /// <summary>
        /// Gives the quiz 
        /// </summary>
        public static void GiveQuiz()
        {
            if (HasAnyQuestions())
            {
                var correct = Questions.Aggregate(0, AskQuestion);
                Console.WriteLine($"You got {correct} out of {Questions.Count}");
            }
        }

        /// <summary>
        /// Lists the quiz questions
        /// </summary>
        public static void ListQuestions()
        {
            if (HasAnyQuestions())
            {
                //int questionNum = 0;
                //foreach (var question in Questions)
                //{
                //    questionNum++;
                //    Console.WriteLine($"{questionNum} ({question.Difficulty}): {question.Text}");
                //}
                Console.WriteLine(Questions.Aggregate
                (
                    seed: (CurrentText: "", CurrentIndex: 0),
                    func: (current, next) =>
                    {
                        current.CurrentIndex++;
                        current.CurrentText += $"{current.CurrentIndex} ({next.Difficulty}): {next.Text}\n";
                        return current;
                    }
                ).CurrentText);
            }
        }

        /// <summary>
        /// Flushes the quiz
        /// </summary>
        public static void FlushQuiz()
        {
            if (HasAnyQuestions())
            {
                Questions.RemoveAll(question => !string.IsNullOrEmpty(question.Text));
            }
        }

        private static void AddQuestion(int index)
        {
            if (index < 0 || index >= Questions.Count)
            {
                if (Questions.Count == MaxQuestions)
                {
                    return;
                }
                var (question, answer, difficulty) = GetQuestionParts();
                Questions.Add(new Question(question, answer, difficulty));
            }
            else
            {
                var (question, answer, difficulty) = GetQuestionParts();
                Questions[index] = new Question(question, answer, difficulty);
            }
        }

        private static (string, string, int) GetQuestionParts() => (GetQuestionText(), GetQuestionAnswer(), GetQuestionDifficulty());

        private static int AskQuestion(int currentCorrect, Question question)
        {
            Console.WriteLine(question.Text);
            if (string.Equals(question.Answer, Console.ReadLine(), StringComparison.CurrentCultureIgnoreCase))
            {
                currentCorrect++;
                Console.WriteLine("Correct!");
            }
            else
            {
                Console.WriteLine($"Incorrect: {question.Answer}");
            }

            return currentCorrect;
        }

        private static string GetQuestionText()
        {
            string question;
            do
            {
                Console.WriteLine("What is the question text?");
                question = Console.ReadLine();
            } while (string.IsNullOrEmpty(question));

            return question;
        }

        private static string GetQuestionAnswer()
        {
            string answer;
            do
            {
                Console.WriteLine("What is the answer?");
                answer = Console.ReadLine();
            } while (string.IsNullOrEmpty(answer));

            return answer;
        }

        private static int GetQuestionDifficulty()
        {
            while (true)
            {
                Console.WriteLine("How difficult (1-3)?");
                if (int.TryParse(Console.ReadLine(), out var difficulty) &&
                    difficulty >= 1 &&
                    difficulty <= 3)
                {
                    return difficulty;
                }
            }
        }

        private static int GetQuestionIndex()
        {
            while (true)
            {
                Console.WriteLine("Choose a number of a question listed.");
                Console.WriteLine("Which question would you like to modify?");
                if (int.TryParse(Console.ReadLine(), out var question) &&
                    question > 0 &&
                    question <= Questions.Count)

                {
                    return question - 1;
                }
            }
        }

        private static bool HasAnyQuestions()
        {
            var result = Questions.Any();
            if (!result)
            {
                Console.WriteLine("No questions found.");
            }

            return result;
        }
    }
}
